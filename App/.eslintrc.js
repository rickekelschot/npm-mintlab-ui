const WARN = 'warn';

module.exports = {
  parser: 'babel-eslint',
  extends: [
    '@mintlab/react',
  ],
};
