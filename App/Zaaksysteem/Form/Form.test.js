import { mount } from 'enzyme';
import { Form } from '.';
import { render, spyOn } from '../../test';

const { assign } = Object;

jest.mock('./library/typemap', () => ({}));
jest.mock('../../Material/Icon', () => 'span');
jest.mock('../../Material/TextField', () => 'input');
jest.mock('../../Material/Tooltip', () => 'span');

const form = props => render(Form, assign({
  classes: {},
  set: jest.fn(),
  validate: jest.fn(),
}, props));

/**
 * @test {Form}
 */
xdescribe('The `Form` component', () => {
  describe('has an `onFocus` method that', () => {
    test('saves the target name to the internal state', () => {
      const { instance } = form();
      const name = 'FOOBAR';

      instance.setState = jest.fn();
      instance.onFocus({
        target: {
          name,
        },
      });

      const assertedCallCount = 1;
      const assertedArgument = {
        focus: name,
      };

      spyOn(instance.setState, assertedCallCount, assertedArgument);
    });
  });

  describe('has an `onBlur` method that', () => {
    test('sets the internal `focus` state to `undefined`', () => {
      const { instance } = form();

      instance.setState = jest.fn();
      instance.onBlur();

      const assertedCallCount = 1;
      const assertedArgument = {
        focus: undefined,
      };

      spyOn(instance.setState, assertedCallCount, assertedArgument);
    });
  });

  describe('has an `onChange` method that', () => {
    test('sets the external state', () => {
      const name = 'FOOBAR';
      const value = 'QUUX';
      const { instance, props } = form({
        fieldSets: [{
          fields: [{
            name,
            type: 'text',
            value,
          }],
        }],
      });
      const constructionOffset = props.set.mock.calls.length;

      instance.onChange({
        target: {
          name,
        },
      });

      const callCount = 1;
      const assertedCallCount = (constructionOffset + callCount);
      const assertedArgument = {
        [name]: value,
      };

      spyOn(props.set, assertedCallCount, assertedArgument);
    });
  });

  describe('has a `getFieldSet` method that', () => {
    test('renders nothing if no fields are provided', () => {
      const { instance } = form();
      const fieldSet = instance.getFieldSet({
        title: 'Hello, World!',
        description: 'The quick brown fox jumps over the lazy dog.',
        fields: [],
      });
      const wrapper = mount(fieldSet);

      const actual = wrapper.find('WithStyles(Card)').length;
      const asserted = 0;

      expect(actual).toBe(asserted);
    });

    const options = {
      title: 'Hello, World!',
      description: 'The quick brown fox jumps over the lazy dog.',
      fields: [{
        type: 'text',
      }],
    };

    xtest('renders the `title` prop', () => {
      const { instance } = form();
      const fieldSet = instance.getFieldSet(options);
      const wrapper = mount(fieldSet);

      const actual = wrapper
        .find('Render > WithStyles(Card)')
        .props()
        .title;
      const asserted = options.title;

      expect(actual).toBe(asserted);
    });

    xtest('renders the `description` prop', () => {
      const { instance } = form();
      const fieldSet = instance.getFieldSet(options);
      const wrapper = mount(fieldSet);

      const actual = wrapper
        .find('Render > WithStyles(Card)')
        .props()
        .description;
      const asserted = options.description;

      expect(actual).toBe(asserted);
    });

    xtest('renders the actual form control', () => {
      const { instance } = form();
      const fieldSet = instance.getFieldSet(options);
      const wrapper = mount(fieldSet);

      const actual = wrapper
        .find('TextField TextField')
        .length;
      const asserted = 1;

      expect(actual).toBe(asserted);
    });
  });

  describe('has a `getFieldList` method that returns', () => {
    test('a flattened array of fields', () => {
      const { instance } = form();

      const actual = instance.getFieldList([
        {
          fields: [
            {
              name: 'foo',
              type: 'text',
            },
          ],
        },
        {
          fields: [
            {
              name: 'bar',
              type: 'text',
            },
          ],
        },
      ]);
      const asserted = [
        {
          name: 'foo',
          type: 'text',
        },
        {
          name: 'bar',
          type: 'text',
        },
      ];

      expect(actual).toEqual(asserted);
    });

    test('an empty array if no truthy argument is provided', () => {
      const { instance } = form();

      const actual = instance.getFieldList();
      const asserted = [];

      expect(actual).toEqual(asserted);
    });
  });

  describe('has a `getFormControl` method that', () => {
    test('creates a form control', () => {
      const { instance } = form();
      const formControl = instance.getFormControl({
        type: 'text',
      });
      const wrapper = mount(formControl);

      const actual = wrapper
        .find('input')
        .length;
      const asserted = 1;

      expect(actual).toBe(asserted);
    });
  });

  describe('has a `getModelValue` method that returns', () => {
    test('its second argument if its first argument is not a key of the internal `errors` state', () => {
      const { instance } = form();

      const actual = instance.getModelValue('foo', 'bar');
      const asserted = 'bar';

      expect(actual).toBe(asserted);
    });

    test('`undefined` if its first argument is a key of the internal `errors` state', () => {
      const { instance } = form();

      instance.setState({
        errors: {
          foo: 'LOREMIPSUM',
        },
      });

      const actual = instance.getModelValue('foo', 'bar');
      const asserted = undefined;

      expect(actual).toBe(asserted);
    });
  });

  describe('has a `getFieldByName` method that', () => {
    test('finds a field in the `fieldSets` prop', () => {
      const fieldSets = [
        {
          fields: [
            {
              name: 'BAR',
              type: 'text',
              value: 'BAR',
            },
          ],
        },
        {
          fields: [
            {
              name: 'FOO',
              type: 'text',
              value: 'BAR',
            },
          ],
        },
      ];
      const { instance } = form({
        fieldSets,
      });

      const actual = instance.getFieldByName('FOO');
      const asserted = {
        name: 'FOO',
        type: 'text',
        value: 'BAR',
      };

      expect(actual).toEqual(asserted);
    });
  });

  describe('has a `getNormalizedFormValue` method that', () => {
    test('returns a boolean for a checkbox', () => {
      const { instance } = form();

      const actual = instance.getNormalizedFormValue({
        checked: true,
        constraints: [],
        type: 'checkbox',
      });
      const asserted = true;

      expect(actual).toBe(asserted);
    });

    test('returns a number for number constraints', () => {
      const { instance } = form();

      const actual = instance.getNormalizedFormValue({
        constraints: ['number'],
        type: 'text',
        value: '42',
      });
      const asserted = 42;

      expect(actual).toBe(asserted);
    });

    test('returns a string in all other cases', () => {
      const { instance } = form();

      const actual = instance.getNormalizedFormValue({
        constraints: [],
        type: 'text',
        value: '42',
      });
      const asserted = '42';

      expect(actual).toBe(asserted);
    });
  });

  describe('has a `processFormField` method that', () => {
    test('updates the internal state', () => {
      const { instance } = form();

      instance.processFormField({
        field: {},
        name: 'foo',
        value: 'bar',
      });

      const actual = instance.state;
      const asserted = {
        disabled: false,
        errors: {},
      };

      expect(actual).toEqual(asserted);
    });

    test('updates the external state', () => {
      const store = {};
      const { instance } = form({
        set(data) {
          assign(store, data);
        },
      });

      instance.processFormField({
        field: {},
        name: 'foo',
        value: 'bar',
      });

      const actual = store;
      const asserted = {
        foo: 'bar',
      };

      expect(actual).toEqual(asserted);
    });
  });

  describe('has a `getStateFromFields` method that', () => {
    test('yields an enabled state if no fields are provided', () => {
      const { instance } = form();
      const actual = instance.getStateFromFields();
      const asserted = {
        disabled: false,
        errors: {},
      };

      expect(actual).toEqual(asserted);
    });

    test('yields a disabled state if there is a validation error', () => {
      const fieldSets = [
        {
          fields: [
            {
              name: 'fail',
              type: 'text',
            },
          ],
        },
      ];
      const { instance } = form({
        fieldSets,
        validate() {
          return 'ERROR';
        },
      });
      const actual = instance.getStateFromFields();
      const asserted = {
        disabled: true,
        errors: {
          fail: 'ERROR',
        },
      };

      expect(actual).toEqual(asserted);
    });
  });

  describe('has a `setExternalState` method that', () => {
    test('sets the external state of all fieldSets fields', () => {
      const fieldSets = [
        {
          fields: [
            {
              name: 'fooName',
              type: 'text',
              value: 'fooValue',
            },
          ],
        },
        {
          fields: [
            {
              name: 'barName',
              type: 'text',
              value: 'barValue',
            },
          ],
        },
        {
          fields: [
            {
              name: 'quuxName',
              type: 'text',
              value: 'quuxValue',
            },
          ],
        },
      ];
      const { instance, props } = form({
        fieldSets,
      });
      const constructionOffset = props.set.mock.calls.length;

      instance.setState({
        errors: {
          quuxName: 'no quux',
        },
      });
      instance.setExternalState();

      const callCount = 1;
      const assertedCallCount = (constructionOffset + callCount);
      const assertedArgument = {
        fooName: 'fooValue',
        barName: 'barValue',
        quuxName: undefined,
      };

      spyOn(props.set, assertedCallCount, assertedArgument);
    });
  });

  describe('has a `isDisabled` method that returns', () => {
    test('`false` if the errors dictionary is empty', () => {
      const { instance } = form();
      const errors = {};

      const actual = instance.isDisabled(errors);
      const asserted = false;

      expect(actual).toBe(asserted);
    });

    test('`true` if the errors dictionary is not empty', () => {
      const { instance } = form();
      const errors = {
        something: 'broke',
      };

      const actual = instance.isDisabled(errors);
      const asserted = true;

      expect(actual).toBe(asserted);
    });
  });
});
