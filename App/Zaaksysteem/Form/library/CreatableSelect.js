import { createElement } from 'react';
import Select from '../../Select';

const CreatableSelect = props =>
  createElement(Select, {
    creatable: true,
    ...props,
  });

export default CreatableSelect;
