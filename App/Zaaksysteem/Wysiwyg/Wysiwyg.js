import { createElement } from 'react';
import LazyLoader from '../../Abstract/LazyLoader';

/**
 * @param {Object} props
 * @return {ReactElement}
 */
export const Wysiwyg = props =>
  createElement(LazyLoader, {
    promise: () => import(
      // https://webpack.js.org/api/module-methods/#import
      /* webpackChunkName: "ui.wysiwyg" */
      './library/Wysiwyg'),
    ...props,
  });

export default Wysiwyg;
