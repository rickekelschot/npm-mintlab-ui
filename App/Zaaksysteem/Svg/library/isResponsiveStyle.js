/**
 * `Svg` component utility function.
 *
 * @param {Object} [style]
 * @return {boolean}
 */
export function isResponsiveStyle(style) {
  if (style) {
    return (
      style.hasOwnProperty('paddingLeft')
      || style.hasOwnProperty('paddingTop')
    );
  }

  return false;
}
