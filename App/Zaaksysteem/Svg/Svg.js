import React from 'react';
import classNames from 'classnames';
import {
  getStyle,
  isResponsiveStyle,
  toViewBox,
} from './library';
import styleSheet from './Svg.css';

const DIMENSIONS = 2;

/**
 * DWIM SVG component.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Svg
 * @see /npm-mintlab-ui/documentation/consumer/manual/Svg.html
 *
 * @param {Object} props
 * @param {*} props.children
 * @param {string} [props.className]
 * @param {string} [props.height]
 *   Wrapper height (CSS length or percentage)
 * @param {number|Array<number>} props.viewBox
 *   SVG viewBox, number (square) or array (width, height)
 * @param {string} [props.width]
 *   Wrapper width (CSS length or percentage)
 * @return {ReactElement}
 */
export const Svg = ({ children, className, height, viewBox, width }) => {
  const viewBoxArray = toViewBox(viewBox);
  const style = getStyle(viewBoxArray.slice(DIMENSIONS), width, height, className);
  const classes = classNames(styleSheet.svg, className, {
    [styleSheet.responsive]: isResponsiveStyle(style),
  });

  return (
    <span
      className={classes}
      style={style}
    >
      <svg viewBox={viewBoxArray.join(' ')}>
        {children}
      </svg>
    </span>
  );
};

export default Svg;
