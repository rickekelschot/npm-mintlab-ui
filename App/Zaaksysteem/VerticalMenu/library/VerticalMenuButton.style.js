import { ICON_SELECTOR } from '../../../Material/MaterialUiThemeProvider/library/theme';

/**
 * Style Sheet for the VerticalMenuButton component
 * @param {Object} theme
 * @return {JSS}
 */
export const VerticalButtonStylesheet = ({
  palette: {
    primary,
  },
  mintlab: {
    greyscale,
    radius,
  },
}) => ({
  root: {
    borderRadius: radius.horizontalMenuButton,
    color: greyscale.darkest,
    '&:hover': {
      color: primary.main,
    },
    '$active&:hover': {
      backgroundColor: primary.lighter,
    },
    ':not($active)&:hover': {
      backgroundColor: `${greyscale.offBlack}0D`,
    },
  },
  active: {
    backgroundColor: primary.lighter,
    color: primary.main,
  },
  label: {
    paddingRight: '17px',
    [ICON_SELECTOR]: {
      margin: '-3px 18px 0 0',
    },
  },
});
