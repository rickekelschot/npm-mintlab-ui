import backgroundColorOption from '../library/backgroundColorOption';

/**
 * @param {Object} parameters
 * @param {Object} parameters.theme
 * @param {boolean} parameters.hasFocus
 * @return {JSS}
 */
const selectStyleSheet = ({
  theme: {
    palette: {
      primary,
      common,
    },
    typography: {
      fontFamily,
    },
    mintlab: {
      greyscale,
      radius,
    },
    typography,
  },
  hasFocus,
}) => {

  const color = hasFocus ? primary.dark : greyscale.offBlack;
  const colorRules = {
    color,
    '&:hover': {
      color,
    },
  };

  return {
    container(base) {
      return {
        ...base,
        width: '100%',
        fontFamily,
        fontWeight: typography.fontWeightLight,
      };
    },
    indicatorSeparator(base) {
      return {
        ...base,
        display: 'none',
      };
    },
    control(base) {
      return {
        ...base,
        height: '36px',
        minHeight: '36px',
        boxShadow: 'none',
        border: 'none',
        borderBottom: 'none',
        backgroundColor: hasFocus ? primary.light : greyscale.light,
        borderRadius: radius.genericInput,
        paddingLeft: '16px',
        '& .startAdornment': {
          ...colorRules,
          margin: '3px 8px 0px 0px',
        },
      };
    },
    valueContainer(base) {
      return {
        ...base,
      };
    },
    placeholder(base) {
      return {
        ...base,
        ...colorRules,
        opacity: 0.5,
      };
    },
    dropdownIndicator(base) {
      return {
        ...base,
        ...colorRules,
        padding: '0px 4px',
      };
    },
    clearIndicator(base) {
      return {
        ...base,
        ...colorRules,
        padding: 0,
      };
    },
    singleValue(base) {
      return {
        ...base,
        ...colorRules,
      };
    },
    input(base) {
      return {
        ...base,
        ...colorRules,
        input: {
          fontWeight: typography.fontWeightLight,
          fontFamily,
        },
      };
    },
    option(base, state) {
      return {
        ...base,
        backgroundColor: backgroundColorOption({ state,  primary, common, greyscale }),
        color: greyscale.offBlack,
      };
    },
  };
};


export default selectStyleSheet;
