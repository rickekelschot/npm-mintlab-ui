import { CreatableSelect } from './CreatableSelect';
import { createEvent, render, spyOn } from '../../../test';

const { assign } = Object;

const name = 'TestCreatable';
const type = 'creatable';

const creatableSelect = props => render(CreatableSelect, assign({
  classes: {},
  theme: null,
  error: 'My Tragic Error',
  hasFocus: true,
  name,
}, props));

/**
 * @test {CreatableSelect}
 */
xdescribe('The `CreatableSelect` component', () => {
  describe('has a `handleFocus` method that', () => {
    test('stops event propagation', () => {
      const { instance } = creatableSelect();
      const event = createEvent('focus');

      instance.handleFocus(event);

      const expectedCallCount = 1;

      spyOn(event.stopPropagation, expectedCallCount);
    });

    test('calls the `onFocus` prop', () => {
      const { instance, props, state } = creatableSelect({
        onFocus: jest.fn(),
      });
      const event = createEvent('focus');

      instance.handleFocus(event);

      const expectedCallCount = 1;
      const expectedArgument = {
        target: {
          name,
          value: state.value,
        },
      };

      spyOn(props.onFocus, expectedCallCount, expectedArgument);
    });
  });

  describe('has a `handleBlur` method that', () => {
    test('stops event propagation', () => {
      const { instance } = creatableSelect();
      const event = createEvent('change');

      instance.handleBlur(event);

      const expectedCallCount = 1;

      spyOn(event.stopPropagation, expectedCallCount);
    });

    test('calls the `onBlur` prop', () => {
      const { instance, props } = creatableSelect({
        onBlur: jest.fn(),
      });
      const event = createEvent('change');

      instance.handleBlur(event);

      const expectedCallCount = 1;
      const expectedArguments = {
        target: {
          name,
          type,
        },
      };

      spyOn(props.onBlur, expectedCallCount, expectedArguments);
    });
  });

  describe('has a `handleChange` method that', () => {
    test('sets the internal state', () => {
      const { instance } = creatableSelect();
      const value = 'Hello, World!';

      instance.setState = jest.fn();
      instance.handleChange(value);

      const expectedCallCount = 1;
      const expectedArgument = {
        value,
      };

      spyOn(instance.setState, expectedCallCount, expectedArgument);
    });

    test('calls the `onChange` prop', () => {
      const { instance, props } = creatableSelect({
        onChange: jest.fn(),
      });
      const value = 'Hello, world!';

      instance.handleChange(value);

      const expectedCallCount = 1;
      const expectedArgument = {
        target: {
          name,
          type,
          value,
        },
      };

      spyOn(props.onChange, expectedCallCount, expectedArgument);
    });
  });
});
