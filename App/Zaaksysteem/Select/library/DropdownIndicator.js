import React from 'react';
import { components } from 'react-select';
import { callOrNothingAtAll } from '@mintlab/kitchen-sink';
import DropdownIndicatorButton from '../../../Shared/DropdownIndicator/DropdownIndicator';

/**
 * @param {Object} props
 * @return {ReactElement}
 */
const DropdownIndicator = props => {
  const {
    getChoices,
    choices,
    selectProps: {
      menuIsOpen,
    },
  } = props;

  // ZS-FIXME: cyclomatic complexity
  /* eslint complexity: [2, 4] */

  const onClick = () => {

    const emptyChoices = () =>
      Array.isArray(choices) && !choices.length;

    const noChoices = !choices || emptyChoices();

    if (menuIsOpen && noChoices) {
      callOrNothingAtAll(getChoices, () => ['']);
    }

  };

  // ZS-FIXME: a11y
  /* eslint jsx-a11y/click-events-have-key-events: warn */
  /* eslint jsx-a11y/no-static-element-interactions: warn */
  return (
    <div onClick={onClick}>
      <components.DropdownIndicator {...props} >
        <DropdownIndicatorButton />
      </components.DropdownIndicator>
    </div>
  );

};

export default DropdownIndicator;
