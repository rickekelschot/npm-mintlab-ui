/**
 * Style Sheet for the PermanentDrawer component
 * 
 * @return {JSS}
 */
export const permanentDrawerStyleSheet = () => ({
  paper: {
    position: 'static',
    background: 'transparent',
    border: 'none',
    padding: '8px 2px',
  },
  list: {
    margin: 0,
    padding: 0,
    listStyle: 'none',
  },
});
