import React from 'react';
import Render from '../../../../Abstract/Render';
import Banner from '../../../Banner';
import Map from '../../../../Abstract/Map';

const { values } = Object;

/**
 * @param {String} className
 * @param {Object} banners
 *    An object, where the keys represents a banner's identifier,
 *    and the value is a valid set of Banner props.
 * @return {ReactElement}
 */
export const Banners = ({
  className,
  banners,
}) => {
  const bannersList = values(banners);

  return (
    <Render condition={bannersList.length}>
      <div className={className}>
        <Map
          data={bannersList}
          component={Banner}
        />
      </div>
    </Render>
  );
};

export default Banners;
