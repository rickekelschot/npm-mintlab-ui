import React from 'react';
import { iterator } from '../library/iterator';
import style from './Circle.css';

const LENGTH = 12;

/**
 * Spinner type for the {@link Loader} component
 * @param {Object} props
 * @param {string} props.color
 * @return {ReactElement}
 */
export const Circle = ({ color }) => (
  <div className={style['circle']}>
    {iterator(LENGTH).map(item => (
      <div
        key={item}
        className={style['child']}
      >
        <span
          className={style['dot']}
          style={{
            backgroundColor: color,
          }}
        />
      </div>
    ))}
  </div>
);
