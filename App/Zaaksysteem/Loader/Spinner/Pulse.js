import React from 'react';
import style from './Pulse.css';

/**
 * Spinner type for the {@link Loader} component
 * @param {Object} props
 * @param {string} props.color
 * @return {ReactElement}
 */
export const Pulse = ({ color }) => (
  <div
    className={style['pulse']}
    style={{
      backgroundColor: color,
    }}
  />
);
