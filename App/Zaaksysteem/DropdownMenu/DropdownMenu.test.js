import React from 'react';
import { mount } from 'enzyme';
import DropdownMenu from '.';

/**
 * @test {DropdownMenu}
 */
describe('The `DropdownMenu` component', () => {
  xtest('renders a button for each item', () => {
    const items = [
      { label: 'foo' },
      { label: 'bar' },
    ];
    const wrapper = mount(
      <DropdownMenu
        items={items}
      >
        <b>Toggle</b>
      </DropdownMenu>
    );
    const actual = wrapper.html();
    const expected = 'TODO';

    expect(actual).toBe(expected);
  });
});
