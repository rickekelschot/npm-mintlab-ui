/**
 * Style Sheet for the BannerButton component
 * @return {JSS}
 */
export const bannerButtonStylesheet = ({
  palette,
  mintlab: {
    radius,
  },
  typography,
}) => ({
  root: {
    color: palette.common.white,
    padding: '0px 18px',
    height: '26px',
    borderRadius: radius.bannerButton,
    fontWeight: typography.fontWeightMedium,
    fontSize: '14px',
    lineHeight: '0px',
  },
  'variant-primary': {
    backgroundColor: palette.primary.main,
  },
  'variant-secondary': {
    backgroundColor: palette.secondary.main,
  },
  'variant-review': {
    backgroundColor: palette.review.main,
  },
  'variant-danger': {
    backgroundColor: palette.danger.main,
  },
});
