import React from 'react';
import ButtonBase from '@material-ui/core/ButtonBase';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { bannerButtonStylesheet } from './BannerButton.style';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} props.action
 * @param {ReactElement} props.children
 * @param {string} props.variant
 * @return {ReactElement}
 */
export const BannerButton = ({
  classes,
  action,
  children,
  variant,
}) => (
  <ButtonBase
    classes={{
      root: classNames(
        classes.root,
        { [classes[`variant-${variant}`]]: true }
      ),
    }}
    onClick={action}
  >
    {children}
  </ButtonBase>
);

export default withStyles(bannerButtonStylesheet)(BannerButton);
