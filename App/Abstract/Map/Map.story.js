import {
  React,
  stories,
} from '../../story';
import Map from '.';

stories(module, __dirname, {
  Default() {
    const listData = [
      { label: 'Foo' },
      { label: 'Bar' },
    ];
    const ListItem = ({
      label,
    }) => (
      <li>{label}</li>
    );

    return (
      <ul>
        <Map
          data={listData}
          component={ListItem}
        />
      </ul>
    );
  },
});
