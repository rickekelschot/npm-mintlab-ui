import { cloneElement } from 'react';
import { cloneWithout } from '@mintlab/kitchen-sink';

/**
 * Use a *JSX* component instead of (yet) an(other) expression
 * to clone properties to a component’s first child.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Abstract/Clone
 * @see /npm-mintlab-ui/documentation/consumer/manual/Clone.html
 *
 * @param {Object} props
 * @param {ReactElement} props.children
 *   Component content nodes
 * @return {ReactElement}
 */
export const Clone = props => {
  const { children } = props;
  const newProps = cloneWithout(props, 'children');

  return cloneElement(children, newProps);
};

export default Clone;
