import React from 'react';
import { shallow } from 'enzyme';
import Clone from '.';

/**
 * @test {Clone}
 */
describe('The `Clone` component', () => {
  test('passes its props into its child component', () => {
    const Test = props => {
      expect(props)
        .toEqual({
          foo: 'foo',
          bar: 'bar',
        });

      return null;
    };

    shallow(
      <Clone
        foo="foo"
        bar="bar"
      >
        <Test/>
      </Clone>
    ).render();
  });
});
