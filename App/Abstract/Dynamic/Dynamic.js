import { createElement } from 'react';
import { cloneWithout } from '@mintlab/kitchen-sink';

/**
 * Pass a component dynamically with a property.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Abstract/Dynamic
 * @see /npm-mintlab-ui/documentation/consumer/manual/Dynamic.html
 *
 * @param {Object} props
 * @return {ReactElement}
 */
export const Dynamic = props => {
  const { component } = props;
  const newProps = cloneWithout(props, 'component');

  return createElement(component, newProps);
};

export default Dynamic;
