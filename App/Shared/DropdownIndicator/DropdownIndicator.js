import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { dropdownIndicatorStylesheet } from './DropdownIndicator.style';
import Icon from '../../Material/Icon/Icon';
import IconButton from '@material-ui/core/IconButton';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} [props.action]
 * @return {ReactElement}
 */
const DropdownIndicator = ({
  classes,
  action,
}) => (
  <IconButton
    onClick={action}
    color='inherit'
    classes={classes}
  >
    <Icon
      size='small'
    >arrow_drop_down</Icon>
  </IconButton>
);

export default withStyles(dropdownIndicatorStylesheet)(DropdownIndicator);
