/**
 * Note: do not set any colors here, this component must inherit its parents colors. *
 * 
 * @return {JSS}
 */
export const dropdownIndicatorStylesheet = () => ({
  root: {
    padding: '3px',
  },
});
