import React, { createElement } from 'react';
import Render from './../../../Abstract/Render';
import { withStyles } from '@material-ui/core/styles';
import IconButton from './PaginationActionsIconButton';
import { pageButtonStyleSheet } from '../Pagination.style';
import { getButtonConfig } from './functions';

const FIRSTPAGE = 0;
/**
 * The page navigation, consistent of:
 * - buttons to step X back,
 * - buttons of surrounding plus current page,
 * - buttons to step X forward
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Number} props.page
 * @param {Number} props.pageCount
 * @param {Function} props.onChangePage
 * @return {ReactElement}
 */
export const TablePaginationActions = props => {
  const {
    classes,
    page,
    pageCount,
    onChangePage,
  } = props;
  const getButtons = name =>
    getButtonConfig(name, page, pageCount, classes)
      .map((button, key) => createElement(IconButton, {
        key,
        button,
        onChangePage,
        page,
        pageCount,
      }));

  return (
    <div
      className={classes.pageButtonsWrapper}
    >
      <Render condition={page !== FIRSTPAGE}>
        <div>
          {getButtons('stepBackButtons')}
        </div>
      </Render>
      <div
        className={classes.pageJumpButtons}
      >
        {getButtons('pageJumpButtons')}
      </div>
      <div>
        {getButtons('stepForwardButtons')}
      </div>
    </div>
  );
};

export default withStyles(pageButtonStyleSheet, {
  withTheme: true,
})(TablePaginationActions);
