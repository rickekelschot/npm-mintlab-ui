import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TablePagination from '@material-ui/core/TablePagination';
import { cloneWithout } from '@mintlab/kitchen-sink';
import PaginationActions from './library/PaginationActions';
import { paginationStyleSheet } from './Pagination.style';

// ZS-TODO: check MUI documentation
// `onChangePage` is never called but required
// (we pass it to `ActionsComponent`)
const noop = () => {};

/**
 * Facade for *Material UI* `TablePagination`.
 *
 * @param {Object} props
 * @param {Function} props.changeRowsPerPage
 * @param {Object} props.classes
 * @param {String} props.component
 * @param {Number} props.count
 * @param {Function} props.getNewPage
 * @param {String} props.labelDisplayedRows
 * @param {Number} props.page
 * @param {Number} props.rowsPerPage
 * @param {Array} props.rowsPerPageOptions
 * @return {ReactElement}
 */
export const CustomPaginationActionsTable = ({
  changeRowsPerPage,
  classes,
  component,
  count,
  getNewPage,
  labelRowsPerPage,
  labelDisplayedRows,
  page,
  rowsPerPageOptions,
  rowsPerPage,
}) => {
  const selectProps = {
    MenuProps: {
      MenuListProps: {
        classes: {
          root: classes.menuList,
        },
      },
      classes: {
        paper: classes.menu,
      },
    },
  };
  const pageCount = Math.ceil(count / rowsPerPage);

  // note: SelectProps and ActionsComponent are UpperCamelCase
  return (
    <TablePagination
      classes={cloneWithout(classes, 'menuList', 'menu')}
      component={component}
      count={count}
      labelDisplayedRows={labelDisplayedRows}
      labelRowsPerPage={labelRowsPerPage}
      onChangeRowsPerPage={changeRowsPerPage}
      onChangePage={noop}
      page={page}
      rowsPerPage={rowsPerPage}
      rowsPerPageOptions={rowsPerPageOptions}
      ActionsComponent={() => (
        <PaginationActions
          onChangePage={getNewPage}
          page={page}
          pageCount={pageCount}
        />
      )}
      SelectProps={selectProps}
    />
  );
};

export default withStyles(paginationStyleSheet)(CustomPaginationActionsTable);
