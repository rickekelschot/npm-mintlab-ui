import {
  React,
  stories,
  action,
  text,
} from '../../story';
import Dialog from '.';

stories(module, __dirname, {
  Default() {
    return (
      <Dialog
        open={true}
        title={text('Title', 'Dialog title')}
        text={text('Text', [
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
          'Vivamus vitae urna dignissim lacus auctor euismod.',
          'Suspendisse auctor vehicula tellus, quis iaculis dolor.',
          'Curabitur commodo lorem eget ex egestas cursus.',
          'Integer dolor nibh, tincidunt mattis pharetra et, gravida ornare mi.',
          'Cras blandit nisi nec efficitur commodo.',
        ].join(' '))}
        buttons={[
          {
            text: 'OK',
            onClick: action('OK'),
            presets: ['primary', 'contained'],
          },
          {
            text: 'Cancel',
            onClick: action('Cancel'),
            presets: ['secondary', 'contained'],
          },
        ]}
      />
    );
  },
});
