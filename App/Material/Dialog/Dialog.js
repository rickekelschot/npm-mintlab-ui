import React from 'react';
import MuiDialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { unique } from '@mintlab/kitchen-sink';
import Map from '../../Abstract/Map';
import Render from '../../Abstract/Render';
import { DialogAction } from './library/DialogAction';

/**
 * *Material Design* **Dialog**.
 * - facade for *Material-UI* `Dialog`, `DialogTitle`,
 *   `DialogContent`, `DialogContentText` and `DialogActions`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Dialog
 * @see /npm-mintlab-ui/documentation/consumer/manual/Dialog.html
 *
 * @param {Object} props
 * @param {boolean} [props.open=false]
 * @param {Array} props.buttons
 * @param {string} props.text
 * @param {string} props.title
 * @return {ReactElement}
 */
export const Dialog = ({
  open = false,
  buttons,
  text,
  title,
  classes,
}) => {
  const labelId = unique();
  const descriptionId = unique();

  return (
    <MuiDialog
      open={open}
      classes={classes}
      aria-labelledby={labelId}
      aria-describedby={descriptionId}
    >
      <DialogTitle
        id={labelId}
      >{title}</DialogTitle>
      <DialogContent>
        <DialogContentText
          id={descriptionId}
        >{text}</DialogContentText>
      </DialogContent>

      <Render
        condition={buttons && buttons.length}
      >
        <DialogActions>
          <Map
            data={buttons}
            component={DialogAction}
          />
        </DialogActions>
      </Render>
    </MuiDialog>
  );
};

export default Dialog;
