import React, { createElement, Fragment } from 'react';
import IconButton from '@material-ui/core/IconButton';
import MuiButton from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import { CustomPalette } from '../MaterialUiThemeProvider/library/CustomPalette';
import Icon from '../Icon';
import { parseAction, parsePresets } from './library/utilities';
import { buttonSemiContained } from './Button.style';

/**
 * *Material Design* **Button**.
 * - facade for *Material-UI* `Button` and `IconButton`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Button
 * @see /npm-mintlab-ui/documentation/consumer/manual/Button.html
 * @see https://material-ui.com/api/button/
 * @see https://material-ui.com/api/icon-button/
 *
 * @param {Object} props
 * @param {Function|string} props.action
 * @param {*} props.children
 * @param {boolean} [props.disabled=false]
 *   [Disabled State](https://html.spec.whatwg.org/multipage/interactive-elements.html#command-facet-disabledstate)
 * @param {string} [props.label]
 *   Assistive text for floating action and icon buttons
 * @param {Array} [props.presets=[]]
 *   color, fullWidth, mini, size and variant
 * @return {ReactElement}
 */
export function Button({
  action,
  children,
  disabled = false,
  icon,
  label,
  presets = [],
}) {
  const props = parsePresets(presets);
  const { color, variant } = props;

  function createInstance(instanceProps) {
    const compact = {
      fab() {
        return (
          <MuiButton
            color={instanceProps.color}
            aria-label={label}
            onClick={action}
            variant="fab"
          >
            <Icon>{children}</Icon>
          </MuiButton>
        );
      },
      icon() {
        return (
          <IconButton
            aria-label={label}
            color={instanceProps.color}
            disabled={disabled}
            onClick={action}
          >
            <Icon
              size={instanceProps.size}
            >{children}</Icon>
          </IconButton>
        );
      },
    };

    if (compact.hasOwnProperty(variant)) {
      return compact[variant]();
    }

    function getChildren() {
      if (icon) {
        return (
          <Fragment>
            <Icon
              size="extraSmall"
            >{icon}</Icon>
            {children}
          </Fragment>
        );
      }

      return children;
    }

    const baseProps = {
      ...parseAction(action),
      ...instanceProps,
      children: getChildren(),
      disabled,
    };

    if (variant === 'semiContained') {
      const enhance = withStyles(buttonSemiContained);

      const SemiContainedButton = ({ classes }) =>
        createElement(MuiButton, {
          ...baseProps,
          classes,
          variant: 'contained',
        });

      return createElement(enhance(SemiContainedButton));
    }

    return createElement(MuiButton, baseProps);
  }

  if (color === 'review') {
    return createElement(CustomPalette, null, createInstance({
      ...props,
      color: 'primary',
    }));
  }

  if (color === 'danger') {
    return createElement(CustomPalette, null, createInstance({
      ...props,
      color: 'secondary',
    }));
  }

  return createInstance(props);
}

export default Button;
