import { createMuiTheme } from '@material-ui/core/styles';

const { assign } = Object;

/**
 * Mintlab theme greyscale.
 *
 * @type {Object}
 */
const greyscale = {
  black: '#2A2E33',
  offBlack: '#2F3338',
  darkest: '#4E5764',
  evenDarker: '#AEB3B9',
  darker: '#D7D7DB',
  dark: '#F2F2F2',
  light: '#F7F7F8',
  lighter: '#FFFFFF',
};

/**
 * Mintlab theme color palette.
 *
 * Note that `review` and `support` are custom palette properties,
 * and `darker` and `lighter` are custom PaletteIntention properties.
 * They are provided with the palette for convenience, but have no
 * support in the Material-UI theme API.
 *
 * @see https://material-ui.com/customization/themes/#palette
 * @type {Object}
 */
export const palette = {
  primary: {
    darker: '#003F8B',
    dark: '#1D65CC',
    main: '#357DF4',
    light: '#C7E0FF',
    lighter: '#E3EEFF',
    support: '#E5FCFF',
  },
  secondary: {
    darker: '#024B3A',
    dark: '#23AF6D',
    main: '#23D17F',
    light: '#74E2AE',
    lighter: '#D9FFD6',
    support: '#DEFFF0',
  },
  error: {
    dark: '#C62443',
    main: '#FF345B',
    light: '#FBE9E7',
  },
  review: {
    dark: '#D18100',
    main: '#FF9D00',
    light: '#FFF5D9',
    support: '#FFE3DB',
  },
  danger: {
    dark: '#C62443',
    main: '#FF345B',
    light: '#FBE9E7',
    support: '#FFE5E2',
  },
  support: {
    main: '#004EE3',
  },
  common: {
    white: greyscale.lighter,
    black: greyscale.black,
  },
  vivid: {
    1: '#0058FF',
    2: '#F4B949',
    3: '#0AD7E8',
    4: '#088DFF',
    5: '#6066FF',
    6: '#872FFF',
  },
};

/**
 * Mintlab theme radii.
 *
 * @type {Object}
 */
export const radius = {
  button: 8,
  buttonSmall: 5,
  card: 2,
  horizontalMenuButton: 30,
  notificationButton: 30,
  verticalMenuButton: 30,
  bannerButton: 30,
  dialog: 10,
  genericInput: 30,
  sheet: 8,
  snackbar: 30,
  dropdownMenu: 15,
  dropdownButton: 8,
  chip: 12,
  chipRemove: '50%',
  wysiwyg: 8,
  textField: 8,
  toolTip: 8,
  tableRow: 8,
  select: 8,
};

/**
 * Mintlab theme icon sizes.
 *
 * @type {Object}
 */
export const icon = {
  extraSmall: 18,
  small: 23,
  medium: 28,
  large: 36,
};

/**
 * Mintlab theme shadows.
 *
 * @type {Object}
 */
export const shadows = {
  insetTop: 'inset 0.1px 1px 3px 0.1px rgba(0,0,0,0.10)',
  flat: '0.1px 1px 3px 0.1px rgba(0,0,0,0.12)',
  medium: '3px 3px 10px 2px rgba(0,0,0,0.05)',
};

export const ICON_SELECTOR = '& > span:first-child > svg';

/**
 * Mintlab theme typography.
 *
 * @type {Object}
 */
const hBase = {
  fontWeight: 400,
  color: greyscale.black,
};

// inline override for third party configuration
/* eslint id-length: [error, { min: 3, exceptions: [h1, h2, h3, h4, h5, h6] }] */
export const typography = {
  useNextVariants: true,
  h1: assign({ fontSize: '2.187rem' }, hBase),
  h2: assign({ fontSize: '1.875rem' }, hBase),
  h3: assign({ fontSize: '1.5625rem' }, hBase),
  h4: assign({ fontSize: '1.375rem' }, hBase),
  h5: assign({ fontSize: '1.25rem' }, hBase),
  h6: assign({ fontSize: '1.125rem' }, hBase),
};

/**
 * Global overrides for Material-UI components.
 * This is preferable over using `withStyles` in the component facade.
 *
 * @type {Object}
 */
export const overrides = {
  MuiButton: {
    root: {
      borderRadius: radius.button,
      textTransform: 'none',
    },
    label: {
      // ZS-TODO: vertical alignment compensation; better ideas?
      paddingTop: '0.1rem',
      [ICON_SELECTOR]: {
        marginRight: '0.4rem',
      },
    },
    // Opting in for all variants would be very verbose
    // (see also ./CustomPalette.js).
    fab: {
      '& $label': {
        paddingTop: '0',
        [ICON_SELECTOR]: {
          marginRight: '0',
        },
      },
    },
    containedSecondary: {
      color: '#fff',
    },
  },
  MuiDialog: {
    paper: {
      borderRadius: radius.dialog,
    },
  },
  MuiDialogActions: {
    root: {
      '& > button': {
        marginRight: '12px',
      },
    },
  },
  MuiPaper: {
    root: {
      padding: '8px',
    },
  },
  MuiDivider: {
    root: {
      margin: '6px 0px',
    },
  },
};

/**
 * Material-UI theme.
 *
 * @type {Object}
 */
export const theme = createMuiTheme({
  overrides,
  palette,
  typography,
  zIndex: {
    inputLabel: 100,
    statusBar: 1250,
    options: 1260,
  },
  mintlab: {
    greyscale,
    icon,
    radius,
    shadows,
  },
});
