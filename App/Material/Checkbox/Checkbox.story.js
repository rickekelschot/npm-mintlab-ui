import {
  React,
  stories,
  select,
} from '../../story';
import Checkbox from '.';

const stores = {
  Default: {
    checked: false,
  },
};

stories(module, __dirname, {
  Default({ store }) {

    const onChange = ({
      target: {
        checked,
      },
    }) => {
      store.set({
        checked,
      });
    };

    return (
      <div 
        onChange={onChange}
      >
        <Checkbox
          checked={store.state.checked}
          label="test"
          name="foobar"
          color={select('Color', ['primary', 'secondary', 'default'], 'primary')}
        />
      </div>
    );
  },
}, stores);

