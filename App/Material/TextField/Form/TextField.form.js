import React, { Component } from 'react';
import MuiTextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import { unique } from '@mintlab/kitchen-sink';
import classNames from 'classnames';
import { textFieldStyleSheet } from './TextField.form.style';

const DEFAULT_ROWS = 3;

/**
 * *Material Design* **Text field**.
 * - facade for *Material-UI* `TextField`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/TextField
 * @see /npm-mintlab-ui/documentation/consumer/manual/TextField.html
 * @see https://material-ui.com/api/text-field/
 *
 * @reactProps {Object} classes
 * @reactProps {Object} config
 * @reactProps {boolean} [disabled=false]
 * @reactProps {string} [error]
 * @reactProps {string} [info]
 * @reactProps {boolean} [required=false]
 * @reactProps {string} label
 * @reactProps {string} name
 * @reactProps {number} rows
 * @reactProps {string} value
 * @reactProps {boolean} hasFocus
 */
export class TextField extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
  }

  // ZS-FIXME: cyclomatic complexity
  /* eslint complexity: [2, 4] */

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        classes: {
          input,
          inputError,
          inputRoot,
          formHelperError,
          inputLabel,
          inputLabelShrink,
          inputLabelInactive,
          inputLabelError,
          formControl,
        },
        disabled = false,
        error,
        info,
        label,
        name,
        required = false,
        config,
        rows = DEFAULT_ROWS,
        value,
        hasFocus,
      },
    } = this;

    const errorState = Boolean(error && !hasFocus);

    return (
      <MuiTextField
        value={value}
        disabled={disabled}
        error={Boolean(error)}
        helperText={error || info}
        id={unique()}
        label={label}
        name={name}
        required={required}
        multiline={config && config.style === 'paragraph'}
        rows={rows}
        classes={{
          root: formControl,
        }}
        InputProps={{
          classes: {
            root: classNames(inputRoot),
            input: classNames(input, { [inputError]: errorState }),
          },
          disableUnderline: errorState,
        }}
        FormHelperTextProps={{
          classes: {
            root: classNames({ [formHelperError]: errorState }),
          },
        }}
        InputLabelProps={{
          classes: {
            root: classNames(inputLabel, inputLabelInactive, { [inputLabelError]: errorState }),
            shrink: classNames(inputLabel, inputLabelShrink, { [inputLabelError]: errorState }),
          },
        }}
      />
    );
  }
}

export default withStyles(textFieldStyleSheet)(TextField);
