import { getCellClass } from './functions';

/* eslint-disable no-magic-numbers */

const bucket = {
  tableCell: 'foo',
  barCell: 'bar',
  quuxCell: 'quux',
};

describe('The `getCellClass` function', () => {
  test('sets all class names for a single element', () => {
    const actual = getCellClass([
      {
        name: 'bar',
      },
    ], bucket, 0);
    const expected = 'foo bar';
    expect(actual).toEqual(expected);
  });
});
