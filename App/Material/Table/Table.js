import React, { Component } from 'react';
import MuiTable from '@material-ui/core/Table';
import MuiTableHead from '@material-ui/core/TableHead';
import MuiTableBody from '@material-ui/core/TableBody';
import MuiTableFooter from '@material-ui/core/TableFooter';
import MuiTableRow from '@material-ui/core/TableRow';
import MuiTableCell from '@material-ui/core/TableCell';
import { withStyles } from '@material-ui/core/styles';
import Render from '../../Abstract/Render';
import TableBody from './TableBody';
import { tableStyleSheet } from './Table.style';
import { bind } from '@mintlab/kitchen-sink';
import equals from 'fast-deep-equal';

const { assign } = Object;

/**
 * *Material Design* **Table**.
 * - facade for *Material-UI* `Table`, `TableHead`,
 *   `TableBody`, `TableFooter`, `TableRow` and `TableCell`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Table
 * @see /npm-mintlab-ui/documentation/consumer/manual/Table.html
 *
 * @reactProps {*} children
 * @reactProps {Object} classes
 * @reactProps {Array} columns
 * @reactProps {Array|null} rows
 * @reactProps {string} noResultDescription
 * @reactProps {boolean} [checkboxes=false]
 * @reactProps {boolean} [selectable=false]
 * @return {ReactElement}
 */

class Table extends Component {

  constructor(props) {
    super(props);

    this.state = {
      selected: [],
    };

    bind(this, 'mergeRows', 'toggleSelected', 'setSelected', 'handleRowClick');
  }

  componentDidUpdate(prevProps) {
    if (!equals(prevProps.rows, this.props.rows)) {
      this.setState({
        selected: [],
      });
    }
  }

  render() {
    const {
      props: {
        children,
        classes,
        columns,
        noResultDescription,
        checkboxes=false,
        selectable=false,
      },
    } = this;

    return (
      <MuiTable className={classes.table}>
        <MuiTableHead>
          <MuiTableRow>

            <Render condition={checkboxes}>
              <MuiTableCell
                className={classes.tableHeadCell}
              />
            </Render>

            {columns.map(({ label }, index) => (
              <MuiTableCell
                className={classes.tableHeadCell}
                key={index}
              >{label}</MuiTableCell>
            ))}
          </MuiTableRow>
        </MuiTableHead>
        <MuiTableBody>
          <TableBody
            classes={classes}
            columns={columns}
            rows={this.mergeRows()}
            noResultDescription={noResultDescription}
            selected={this.state.selected}
            checkboxes={checkboxes}
            selectable={selectable}
            {...selectable && {onSelectRow: this.handleRowClick} }
          />
        </MuiTableBody>
        <Render
          condition={children}
        >
          <MuiTableFooter>
            <MuiTableRow>
              {children}
            </MuiTableRow>
          </MuiTableFooter>
        </Render>
      </MuiTable>
    );
  }

  /**
   * @param {SyntheticEvent} event
   * @param {*} id
   */
  handleRowClick(event, id) {
    const {
      target: {
        type,
      },
    } = event;

    if (type === 'checkbox') {
      this.toggleSelected(id);
    } else {
      this.setSelected(id);
    }
  }

  /** 
  * Add a row's id to the list of selected rows,
  * or remove it if already present.
  * @param {*} id
  */
  toggleSelected(id) {
    const current = this.state.selected;
    let selected;

    if (current.includes(id)) {
      selected = current.filter(thisId => thisId !== id);
    } else {
      selected = [ ...current, id];
    }
    this.setState({
      selected,
    });
  }

  /**
   * Set a single row's id as selected,
   * deselecting other rows.
   * @param {*} id
   */
  setSelected(id) {
    this.setState({
      selected: [id],
    });
  }

  /**
   * @return {Array}
   */
  mergeRows() {
    const {
      props: {
        rows,
      },
      state: {
        selected,
      },
    } = this;

    if (!rows) {
      return rows;
    }

    return rows.map((row, index) =>
      assign({}, row, { selected: selected.includes(index) })
    );
  }
}

export default withStyles(tableStyleSheet)(Table);
