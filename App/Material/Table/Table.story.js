import {
  React,
  stories,
  boolean,
} from '../../story';
import Table from '.';
import Pagination from './../Pagination/Pagination';

/* eslint-disable no-magic-numbers */

const users = ['Arne', 'Eric', 'Danny'];
const components = ['user', 'case', 'object'];
const ROWS_PER_PAGE_OPTIONS = [5, 10, 25, 50, 100];

const COUNT = 101;
const DATECORRECTION = 1;

const spoofApi = (page, rowsPerPage) => {
  const resultsOnPage = Math.min(rowsPerPage, COUNT - (rowsPerPage * page));
  const rows = [];

  for (let index = 0; index < resultsOnPage; index++) {
    const date = `2018-${page + DATECORRECTION}-${index + DATECORRECTION}`;
    const component = components[(index + page) % components.length];

    const caseNumber = page * rowsPerPage + index + 1;

    rows.push({
      caseNumber,
      date,
      description: 'Something happened',
      component,
      user: users[(index + page) % users.length],
    });
  }

  return rows;
};

const stores = {
  Default: {
    count: COUNT,
    page: 0,
    queue: 0,
    rows: spoofApi(0, 10),
    rowsPerPage: 10,
  },
};

const columns = [
  {
    label: 'Zaak #',
    name: 'caseNumber',
  },
  {
    label: 'Datum',
    name: 'date',
  },
  {
    label: 'Omschrijving',
    name: 'description',
  },
  {
    label: 'Component',
    name: 'component',
  },
  {
    label: 'Behandelaar',
    name: 'user',
  },
];

stories(module, __dirname, {
  Default({ store }) {
    function fetchNewData(page = store.state.page, rowsPerPage = store.state.rowsPerPage) {
      const delay = 2000;

      store.set({
        page,
        queue: (store.state.queue + 1),
        rowsPerPage,
        rows: [],
      });

      function callback() {
        if (store.state.queue === 1) {
          store.set({
            count: COUNT,
            page,
            queue: (store.state.queue - 1),
            rowsPerPage,
            rows: spoofApi(page, rowsPerPage),
          });
        } else {
          store.set({
            queue: (store.state.queue - 1),
          });
        }
      }

      window.setTimeout(callback, delay);
    }

    // ZS-TODO: signature unclear
    // Check the Material UI documentation. The second parameter
    // has a misleading name (seems to be a component instance)
    // and is not documented for the current release.
    const changeRowsPerPage = (event, expectedRowsPerPage) => {
      const { page, rowsPerPage } = store.state;
      const newRowsPerPage = expectedRowsPerPage.props.value;
      const expectedPage = Math.floor(page * rowsPerPage / newRowsPerPage);

      fetchNewData(expectedPage, newRowsPerPage);
    };

    return (
      <Table
        columns={columns}
        getNewPage={fetchNewData}
        page={store.state.page}
        rows={store.state.rows}
        rowsPerPage={store.state.rowsPerPage}
        selectable={boolean('Selectable', true)}
        checkboxes={boolean('Checkboxes', true)}
      >
        <Pagination
          changeRowsPerPage={changeRowsPerPage}
          count={store.state.count}
          getNewPage={fetchNewData}
          labelDisplayedRows={({ count, from, to }) => `${from}–${to} van ${count}`}
          labelRowsPerPage={'Per pagina:'}
          page={store.state.page}
          rowsPerPage={store.state.rowsPerPage}
          rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
        />
      </Table>
    );
  },
}, stores);
