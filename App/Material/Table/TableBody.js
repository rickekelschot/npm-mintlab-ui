import React, { Fragment } from 'react';
import MuiTableRow from '@material-ui/core/TableRow';
import MuiTableCell from '@material-ui/core/TableCell';
import Loader from '../../Zaaksysteem/Loader';
import { getCellClass } from './library/functions';
import { Render } from '../../Abstract/Render';
import MUICheckbox from '../Checkbox';
import classNames from 'classnames';
import { callOrNothingAtAll } from '@mintlab/kitchen-sink';

const ZERO = 0;

/**
 * *Material UI* `TableBody` facade component for table.
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Array} props.columns
 * @param {Array} props.rows
 * @param {string} props.noResultDescription
 * @param {function} [props.onSelectRow]
 * @param {boolean} checkboxes
 * @param {boolean} selectable
 * @return {ReactElement}
 */
export const TableBody = ({
  classes,
  columns,
  rows,
  noResultDescription,
  onSelectRow,
  checkboxes,
  selectable,
}) => {

  if (rows) {
    if (rows.length) {
      return rows.map((row, rowIndex) => (
        <MuiTableRow
          key={rowIndex}
          className={
            classNames(
              classes.tableRow,
              { [classes.pointer]: selectable }
            )
          }
          onClick={thisEvent => callOrNothingAtAll(onSelectRow, () => [thisEvent, rowIndex])}
          selected={row.selected}
          classes={{
            selected: classes.selectedRow,
          }}
        >
          {columns.map(({ name }, columnIndex) => {

            const hasCheckbox = columnIndex === ZERO && checkboxes;

            return (
              <Fragment key={columnIndex}>
                <Render condition={hasCheckbox}>
                  <MuiTableCell
                    key={`${rowIndex}-check`}
                    classes={{
                      root: classNames(classes.tableCell, classes.checkboxCell),
                    }}
                  >
                    <MUICheckbox
                      checked={row.selected}
                      formControlClasses={{
                        root: classes.checkboxFormControlLabel,
                      }}
                    />
                  </MuiTableCell>
                </Render>
                <MuiTableCell
                  key={columnIndex}
                  className={getCellClass(columns, classes, columnIndex)}
                >{row[name]}</MuiTableCell>
              </Fragment>
            );
          }
          )}
        </MuiTableRow>
      ));
    }

    return (
      <MuiTableRow>
        <MuiTableCell
          colSpan={columns.length}
          className={classes.noResult}
        >
          {noResultDescription}
        </MuiTableCell>
      </MuiTableRow>
    );
  }

  const firstIndex = ZERO;

  return (
    <MuiTableRow>
      <MuiTableCell
        colSpan={columns.length}
        className={getCellClass(columns, classes, firstIndex)}
      >
        <Loader />
      </MuiTableCell>
    </MuiTableRow>
  );
};

export default TableBody;
