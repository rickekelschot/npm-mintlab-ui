/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the Table component
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const tableStyleSheet = ({
  palette: {
    primary,
    common,
  },
  mintlab: {
    greyscale,
  },
}) => {

  const hoverSelected = {
    background: primary.lighter,
    borderTop: `1px solid ${common.white}`,
  };

  return {
    table: {},
    tableHeadCell: {
      fontWeight: 'bold',
      color: greyscale.black,
      fontSize: '16px',
      whiteSpace: 'nowrap',
      paddingLeft: '20px',
      paddingRight: '0',
      borderBottom: `1px solid ${common.white}`,
    },
    tableRow: {
      borderTop: `1px solid ${greyscale.dark}`,
      '&:hover': hoverSelected,
      '&:hover + tr': {
        borderTop: `1px solid ${common.white}`,
      },
    },
    tableCell: {
      padding: '20px 0px 20px 20px',
      borderBottom: 'none',
    },
    noResult: {
      textAlign: 'center',
      fontSize: '30px',
      borderBottom: `1px solid ${common.white}`,
    },
    checkboxCell: {
      padding: '0px',
      margin: '0px',
      width: '1%',
    },
    checkboxFormControlLabel: {
      padding: '0px',
      margin: '0px',
    },
    selectedRow: {
      '&&': hoverSelected,
    },
    pointer: {
      '&:hover': {
        cursor: 'pointer',
      },
    },
  };
};
