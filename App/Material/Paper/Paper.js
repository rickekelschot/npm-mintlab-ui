import React from 'react';
import MuiPaper from '@material-ui/core/Paper';

/**
 * Facade for *Material-UI* `Paper`
 * - all `props` are passed through
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Paper
 * @see /npm-mintlab-ui/documentation/consumer/manual/Paper.html
 *
 * @param {Object} props
 * @return {ReactElement}
 */
const Paper = props => {
  const {
    classes,
    children,
    ...rest
  } = props;

  return (
    <MuiPaper
      classes={classes}
      { ...rest }
    >
      {children}
    </MuiPaper>
  );
};

export default Paper;
