import React from 'react';
import { mount } from 'enzyme';
import Paper from '.';

/**
 * @test {Paper}
 */
describe('The `Paper` component', () => {

  test('passes all props to its child component', () => {
    const wrapper = mount(
      <Paper
        elevation={13}
        square={true}
      >:-)</Paper>
    );
    const actual = wrapper
      .find('Paper Paper')
      .props();
    const asserted = {
      children: ':-)',
      elevation: 13,
      square: true,
    };

    expect(actual).toMatchObject(asserted);
  });

});
