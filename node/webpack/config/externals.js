const { readFileSync } = require('fs');
const { join } = require('path');

const { parse } = JSON;
const { keys } = Object;

// Source of truth: package.json peerDependencies; note that
// - `peerDependencies` are just assertions, *not* installation directives
// - webpack `externals` is what actually excludes packages from the build
const packagePath = join(process.cwd(), 'package.json');
const packageContent = readFileSync(packagePath, 'utf8');
const { peerDependencies } = parse(packageContent);
const externals = keys(peerDependencies);

module.exports = externals;
