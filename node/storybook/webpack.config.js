const requireDirectory = require('require-directory');
const userConfig = requireDirectory(module, './webpack');

const { NODE_ENV } = process.env;

function getProductionConfiguration(storybookBaseConfig) {
  const { assign } = Object;

  return assign(storybookBaseConfig, {
    module: {
      rules: [
        ...storybookBaseConfig.module.rules,
        ...userConfig.module.rules,
      ],
    },
    node: userConfig.node,
    resolve: assign(
      {},
      storybookBaseConfig.resolve,
      userConfig.resolve
    ),
  });
}

// This is less hairy then forking based on the environment
// inside the full-control mode function.
module.exports = (NODE_ENV === 'production')
  ? getProductionConfiguration
  : userConfig;
