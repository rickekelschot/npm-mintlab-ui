const { addDecorator, configure } = require('@storybook/react');
const { setOptions } = require('@storybook/addon-options');
const { withKnobs } = require('@storybook/addon-knobs');
const { checkA11y } = require('@storybook/addon-a11y');

setOptions({
  name: 'Zaaksysteem UI',
  url: 'https://quarterly.zaaksysteem.nl',
});

addDecorator(withKnobs);
addDecorator(checkA11y);

const requireAll = requireContext =>
  requireContext
    .keys()
    .map(requireContext);

function loadStories() {
  const context = require.context('../../App', true, /\.story\.jsx?$/);

  requireAll(context);
}

configure(loadStories, module);
