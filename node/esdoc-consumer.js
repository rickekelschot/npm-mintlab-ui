// ESDoc configuration for consumer API documentation
const glob = require('glob');
const del = require('del');

const token = '[A-Z][a-z0-9]+';
const upperCamelCase = `${token}(?:${token})*`;
const source = './App';
const sectionExpression = new RegExp(`/(${upperCamelCase})\\.md`);
const readmeGlob = `${source}/**/*.md`;
const { npm_package_config_PUBLIC } = process.env;
const destination = `./${npm_package_config_PUBLIC}/documentation/consumer`;

function sortReadmes(pathOne, pathTwo) {
  const [, componentOne] = sectionExpression.exec(pathOne);
  const [, componentTwo] = sectionExpression.exec(pathTwo);

  if (componentOne > componentTwo) {
    return 1
  }

  if (componentOne < componentTwo) {
    return -1;
  }

  return 0;
}

const componentReadmes = glob
  .sync(readmeGlob)
  .sort(sortReadmes);

del.sync(destination);

module.exports = {
  source,
  destination,
  includes: [
    '^(esdoc|index).js$',
    `^${upperCamelCase}/${upperCamelCase}/${upperCamelCase}\\.js`,
    // Typography is an exception
    `/Typography/style/${upperCamelCase}\\.js`,
  ],
  plugins: [
    {
      name: 'esdoc-standard-plugin',
      option: {
        brand: {
          title: '@mintlab/ui'
        },
        test: {
          source,
          interfaces: [
            'describe',
            'test',
          ],
          includes: [
            `/${upperCamelCase}\\.test\\.js$`
          ]
        },
        manual: {
          globalIndex: true,
          index: './node/manual/consumer/cover.md',
          files: [
            './node/manual/consumer/installation.md',
            './node/manual/consumer/peer-dependencies.md',
            './node/manual/consumer/usage.md',
            ...componentReadmes,
          ],
        },
      },
    },
    {
      name: 'esdoc-importpath-plugin',
      option: {
        replaces: [
          {
            from: '.*',
            to: '',
          },
        ],
      },
    },
    {
      name: 'esdoc-jsx-plugin',
    },
    {
      name: 'esdoc-react-plugin',
    },
    {
      name: 'esdoc-ecmascript-proposal-plugin',
      option: {
        dynamicImport: true,
        objectRestSpread: true,
      },
    },
  ],
};
