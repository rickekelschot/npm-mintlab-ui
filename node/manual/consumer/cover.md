[⇱ Table of contents](../../)

<hr>

# `@mintlab/ui` consumer documentation 

> `@mintlab/ui` is an `npm` package with the 
  [presentational](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0)
  React UI components used in Zaaksysteem.

## See also

- [Developer Documentation](../developer/)
- [Storybook](../../storybook/)
